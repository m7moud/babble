import React from 'react'
import Router from 'react-router'
import Chat from './components/chat'
import Home from './components/home'
import Channel from './components/channel'
let { Route, Link, Redirect } = Router;


let routes = (
	<Route handler={Chat}>
		<Route path="/" handler={Home} />
		<Redirect from="/*/" to="/*" />
		<Route path="*" handler={Channel} />
	</Route>
);


Router.run(routes, Router.HistoryLocation, (Handler, State) => {
	React.render(<Handler />, document.getElementById('react-container'));
});
