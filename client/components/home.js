import React from 'react'
import Router from 'react-router'
let { Route, Link, Redirect, Navigation, State } = Router;
import Header from './header'
class Home extends React.Component {
  contextTypes: {
    router: React.PropTypes.func.isRequired
  }
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();
    this.context.router.replaceWith('/' + React.findDOMNode(this.refs.room).value);
  }
  render() {
    return (
        <div className="well">
          <h4>Welcome to Babbel{(this.context.username)? ', ' + this.context.username + "!" : "" }</h4>
          <form className="form-inline" onSubmit={this.handleSubmit}>
          <p>You can join an existing room or create a new one</p>
            <div className="input-group">
              <input autoFocus="{true}" ref="room" type="text" className="form-control" placeholder="Room" />
              <span className="input-group-btn">
                <button type="submit" className="btn btn-success">Join</button>
              </span>
            </div>
          </form>
        </div>
    );
  }
}

Home.contextTypes = {
  router: React.PropTypes.func.isRequired,
  username: React.PropTypes.string.isRequired
};
export default Home;
