import React from 'react'
import {ReconnectingWebSocket} from 'awesome-websocket'
import Chatlog from './chatlog'
import Chatbox from './chatbox'

class Channel extends React.Component {
    constructor() {
      super();
      this.state = {
        connected: false,
        name: '',
        users: [],
        user: {},
        messages: []
      };
      this.handleMessageSubmit = this.handleMessageSubmit.bind(this);
    }
    componentWillMount() {
      var channel_name = this.context.router.getCurrentPath().substring(1);
      this.socket = new ReconnectingWebSocket(location.origin.replace(/^http/, 'ws') + '/ws/'+ channel_name);
      this.socket.onopen = () => {
        this.setState({
          connected: true,
          users: this.state.users,
          name: channel_name
        });
        this.socket.keepAlive(60 * 1000, "ping!");
      };
      this.socket.onmessage = (e) => {
        this.state.messages.push(JSON.parse(e.data));
        this.setState({messages: this.state.messages});
      };
      this.socket.onclose = () => {
        this.setState({connected: false});
      };
    }
    componentWillUnmount() {
      this.socket.close();
    }
    handleMessageSubmit(message){
      this.socket.send(JSON.stringify({m: message, u: this.context.username}));
    }
  render() {
    return (
      <div className="chat-form">
      <Chatlog messages={this.state.messages} />
      <Chatbox submitfnc={this.handleMessageSubmit} connected={this.state.connected} />
      </div>
    );
  }
}
Channel.contextTypes = {
  router: React.PropTypes.func.isRequired,
  username: React.PropTypes.string.isRequired
};
export default Channel;
