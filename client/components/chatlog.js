import React from 'react'
import Message from './message'

class Chatlog extends React.Component {
  constructor() {
    super();
    this.renderMessage = this.renderMessage.bind(this);
  }
  renderMessage(message, i) {
    var props = {
      key: i,
      message: message
    };
    return (
      <li key={message.u + "-" + message.t} id={(this.props.messages.length == (i + 1))? 'last' : ''}>
        <Message {...props} />
      </li>
    )
  }
  componentDidMount() {
    document.getElementById('chatbox').focus();
  }
  render() {
    return <ul className="chat" onChange={this.handleChange}>{this.props.messages.map(this.renderMessage)}</ul>;
  }
}
export default Chatlog;
