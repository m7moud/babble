import React from 'react'

class Login extends React.Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.loginfnc(React.findDOMNode(this.refs.username).value);
  }
  render() {
    return (
      <div className="well">
        <form className="form-inline" onSubmit={this.handleSubmit}>
          <p>Choose a Username to go</p>
          <div className="input-group">
            <input autoFocus="{true}" ref="username" type="text" className="form-control" placeholder="Username" />
            <span className="input-group-btn">
              <button type="submit" className="btn btn-success">Login</button>
            </span>
          </div>
        </form>
      </div>
    )
  }
}
Login.contextTypes = {
  router: React.PropTypes.func.isRequired
};
export default Login;
