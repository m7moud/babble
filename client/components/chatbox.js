import React from 'react'

class Chatbox extends React.Component {
  constructor() {
    super();
    this.state = {
      text: ''
    };
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleKeyUp(e) {
    if (e.keyCode === 13 && !e.ctrlKey) {
      if (React.findDOMNode(this.refs.textarea).value.trim().length > 0) {
        this.props.submitfnc(React.findDOMNode(this.refs.textarea).value.replace(/(\s*(\r?\n|\r))+$/, ''))
        this.setState({
          text: ''
        });
      }
    } else if (e.keyCode === 13 && e.ctrlKey) {
      this.setState({
        text: React.findDOMNode(this.refs.textarea).value + '\n'
      });
    }
  }
  handleChange() {
    this.setState({
      text: React.findDOMNode(this.refs.textarea).value
    });
  }
  render() {
    return (
      <textarea id="chatbox" spellCheck="true" autoFocus="true" className="form-control" onKeyUp={this.handleKeyUp} onChange={this.handleChange} ref="textarea" value={this.state.text} disabled={!this.props.connected}></textarea>
    )
  }
}
export default Chatbox;
