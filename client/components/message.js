import React from 'react'
import SmartTimeAgo from 'react-smart-time-ago'

class Message extends React.Component {
  componentDidMount() {
    if (document.getElementById('last')){
      document.getElementById('last').scrollIntoView();
    }
  }
  rawMarkup() {
    var rawMarkup = this.props.message.m.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + "<br>" + '$2');
    return { __html: rawMarkup };
  }
  render() {
    return (
      <span className="message">
        <strong className="from">{this.props.message.u}</strong>
        <SmartTimeAgo className="time pull-right" value={new Date(this.props.message.t)} />
        <span className="text" dangerouslySetInnerHTML={this.rawMarkup()}></span>
      </span>
    )
  }
}
export default Message;
