import React from 'react'
import Router from 'react-router'
import Header from './header'
import Footer from './footer'
import Login from './login'
var { RouteHandler } = Router;

class Chat extends React.Component {
  constructor() {
    super();
    this.state = {
      username: '',
      loggedIn: false
    };
    if (localStorage.username){
      this.state = {
        username: localStorage.username,
        loggedIn: true
      };
    } else {
      this.state = {
        username: '',
        loggedIn: false
      };
    }
    this.login = this.login.bind(this);
  }
  getChildContext() {
    return { username: this.state.username };
  }
  login(username) {
    if (username.length){
      localStorage.username = username;
      this.setState({username: username, loggedIn: true});
    }
  }
  render() {
    return (
      <div>
        <Header />
        {(this.state.loggedIn)? <RouteHandler /> : <Login loginfnc={this.login} />}
        <Footer />
      </div>
    );
  }
}
Chat.childContextTypes = {
  username: React.PropTypes.string.isRequired
};

export default Chat;
