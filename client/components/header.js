import React from 'react'
import { Route, Link } from 'react-router'

class Header extends React.Component {
  render() {
    return (
      <div className="clearfix">
        <Link to="/" className="header"><h1 className="pull-left">Babble</h1></Link>
        <small className="channel-name pull-right">{(this.context.router.getCurrentPath() != '/')? "#" + this.context.router.getCurrentPath().substring(1) : ""}</small>
      </div>
    )
  }
}
Header.contextTypes = {
  router: React.PropTypes.func.isRequired,
  username: React.PropTypes.string.isRequired
};
export default Header;
