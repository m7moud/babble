var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	entry: [
		"./client.js",
		"bootstrap-webpack!./bootstrap.config.js",
		"./styles.css"
	],
	output: {
		path: '../public/assets',
		filename: "bundle.js"
	},
	module: {
		loaders: [
			{ exclude: /(node_modules|bootstrap.config.js|styles.css)/, loader: 'babel' },
			{ test: /\.(coffee\.md|litcoffee)$/, loader: "coffee-loader?literate" },
			{ test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader") },
			{ test: /\.(woff|woff2)$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
			{ test: /\.ttf$/, loader: "file-loader" },
			{ test: /\.eot$/, loader: "file-loader" },
			{ test: /\.svg$/, loader: "file-loader" }
		]
	},
	plugins: [
		new ExtractTextPlugin("styles.css")
	]
};
