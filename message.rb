class Message
  include Mongoid::Document
  field :u, type: String
  field :m, type: String
  field :t, type: Time
  field :c, type: String
  validates_presence_of :u, :m, :t, :c

  def as_json(options = nil)
    {
      u: u,
      m: m,
      t: t
    }
  end
end
