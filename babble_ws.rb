require 'bundler'
Bundler.require
Mongoid.load!("./config/mongoid.yml", ENV['RACK_ENV'])
require './message'

class BabbleWs < Rack::WebSocket::Application
  def initialize
    EM.next_tick { $redis = EM::Hiredis.connect }
  end

  def on_open(env)
    puts 'Client connected'
    @channel = env["PATH_INFO"][1..-1].downcase
    $redis.pubsub.subscribe(@channel).callback { puts "PubSub Subscribed" }
    Message.where(c: @channel).order_by(:t.desc).limit(30).reverse.each do |msg|
      send_data msg.to_json
    end
    $redis.pubsub.on(:message) do |channel, message|
      send_data message if channel.eql?(@channel)
    end
  end

  def on_message(env, msg)
    @channel = env["PATH_INFO"][4..-1].downcase
    msg = Message.new(JSON.parse(msg).merge({c: @channel, t: Time.now}))
    $redis.publish(@channel, msg.to_json)
    msg.save
  end

  def on_close(env)
    @channel = env["PATH_INFO"][4..-1].downcase
    puts 'Client disconnected'
    $redis.pubsub.unsubscribe(@channel).callback { puts "PubSub Unsubscribed" }
  end
end
