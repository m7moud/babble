require 'rack'
require 'rack/websocket'
require ::File.expand_path('../babble_ws', __FILE__)

map '/' do
  use Rack::Static,
  :urls => ["/assets"],
  :root => "public"

  run lambda { |env|
    [
      200,
      {
        'Content-Type'  => 'text/html',
        'Cache-Control' => 'public, max-age=86400'
      },
      File.open('public/index.html', File::RDONLY)
    ]
  }
end
map '/ws' do
  run BabbleWs.new
end
