# Babble
Simple chat application implemented with Ruby, WebSocket, React on ES6, Redis and MongoDB.

You can chat on multiple channels/room, the chat log is persisted on MongoDB and Redis is used to mange the Pub/Sub on these different channels. and the user information is stored on browser localStorage.

You can find use it in here: [https://nameless-sea-5603.herokuapp.com/](https://nameless-sea-5603.herokuapp.com/)

## Dependencies Installation
`bundle install`

## Usage

```
foreman start -f Procfile.dev
```

then head to: [http://localhost:5000/](http://localhost:5000/)

## License
Copyright (c) 2015 by Mahmoud Ali
